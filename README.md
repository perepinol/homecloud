# Homecloud

Homecloud is my personal home lab, running on K3s on a Raspberry Pi 4B. It contains the following services:

* [pi-hole](https://pi-hole.net/)
* [miclub-reservations](https://gitlab.com/perepinol/miclub-reservations)

## Setup

This section contains the steps required to get the cluster working, from burning Raspbian into the SD card to running Pihole's DHCP and DNS server.

### Preinstallation

Burn Raspbian into an SD card through the Raspberry Pi Imager. Configure it so that the SSH service is enabled on startup.

### Raspberry setup

**Note**: Pihole will end up acting as your DHCP server, but you may want to keep another server enabled until you have confirmed that the Raspberry has a static IP address.
This will (ideally) let you configure it through SSH without needing to plug in a screen and keyboard.

#### Installing required utilities

Follow the steps linked below:

* Git: `apt-get install git`
* Docker: [32-bit](https://docs.docker.com/engine/install/raspberry-pi-os/)/[64-bit](https://docs.docker.com/engine/install/debian/)
* [K3s](https://docs.k3s.io/quick-start): if you run into a `Failed to find memory cgroup` error, add `cgroup_memory=1 cgroup_enable=memory` at the end of the first line of `/boot/cmdline.txt`.

#### Networking

Modify [eth0](eth0) to configure your primary interface. Copy it to `/etc/network/interfaces.d/`.

On startup, the Raspberry should have your IP of choice. At this point, the old DHCP server can be disabled, and the Raspberry should still be reachable through SSH (as long as your client has an IP address too).

#### K3s

Copy [traefik-config.yaml](kubernetes/traefik-config.yaml) to `/var/lib/rancher/k3s/server/manifests/`. This will trigger a restart of pods in the `kube-default` namespace if your K3s cluster is already running. Some might get stuck in `Terminating`; you can fiddle with reinstalling K3s and re-copying the file to get it to work.

#### Docker repository

Some of the Kubernetes deployments use a local repository. You can create one with:

`docker run -d -p 5000:5000 --name registry --restart always budry/registry-arm`

#### Pihole

Create a directory `/data/pihole` if you don't have it.

Go to `kubernetes/pihole/dhcp-relay` and build+push the image there:

`docker build -t localhost:5000/dhcp-relay . && docker push localhost:5000/dhcp-relay`

Go to `kubernetes/pihole` and run `make start-support`. This will create a namespace and set up the Persistent Volume for `/data/pihole`. Then create a secret with the following variables:

```
TZ="Europe/Paris"
WEBPASSWORD="yourpassword"
```

You can do this by writing the variables in a file and executing:

`kubectl create secret generic -n pihole pihole --from-file=/path/to/file`

Then run `make start` to set up the deployments, services and ingress. This makes pihole available through a NodePort for ease of access until DNS is configured.

Once all the pods are running, you should open the Pihole admin portal and configure DHCP and DNS. In particular, you should add a DNS record for the domain name you have configured for Pihole in `kubernetes/pihole/ingress.yml`.

With the new configuration saved, you should be able to obtain a lease from Pihole's DHCP server. You can easily check with another network interface, computer or phone.

## Important locations

* `/data`
* `/opt/k3s` if present.
* This repository, if it contains local changes.
